# Example of drag and drop in vala

Drag files and see them listed on main window treeview

![screencast](etc/screencast.webm)

# Credits

- Based on great blog post: https://laptrinhx.com/vala-9-drag-drop-3521859046/
